﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroDeUsuarios
{
    class RegistroDbContext: DbContext
    {
        public DbSet<Usurios> Usuarios { get; set; }
        public DbSet<Matriculas> Matriculas { get; set; }
        
    }
}
