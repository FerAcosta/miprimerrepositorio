﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroDeUsuarios
{
    class Matriculas
    {
        [Key]
        public int IdMatricula { get; set; }
        public string Matricula { get; set; }

    }
}
