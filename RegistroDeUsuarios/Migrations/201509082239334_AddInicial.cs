namespace RegistroDeUsuarios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Matriculas",
                c => new
                    {
                        IdMatricula = c.Int(nullable: false, identity: true),
                        Matricula = c.String(),
                    })
                .PrimaryKey(t => t.IdMatricula);
            
            CreateTable(
                "dbo.Usurios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellidos = c.String(),
                    })
                .PrimaryKey(t => t.IdUsuario);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Usurios");
            DropTable("dbo.Matriculas");
        }
    }
}
