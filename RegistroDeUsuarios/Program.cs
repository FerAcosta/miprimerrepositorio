﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroDeUsuarios
{
    class Program
    {
        static void Main(string[] args)
        {
            //registro();
            RegMatricula();
            Console.ReadKey();

        }
      
        static void registro()
        {
            Usurios users = new Usurios();

            using (var db = new RegistroDbContext())
            {
                Console.Write("Nombre:");
                string nombre = Console.ReadLine();
                users.Nombre = nombre;
                Console.Write("Apellido:");
                string apellido = Console.ReadLine();
                users.Apellidos = apellido;
                    


                db.Usuarios.Add(users);
                db.SaveChanges();

                Console.Write("Registro OK...");

            }
            
        }
        private static void RegMatricula()
        {
            Matriculas mat = new Matriculas();

            using (var db= new RegistroDbContext())
            {
                Console.Write("Digite el usuario:");
                var usermat = Console.ReadLine();
                var query = (from q in db.Usuarios where q.Nombre == usermat select q).Count();

                if (query == 0)
                {
                    Console.Write("Error");
                    Console.ReadKey();
                }
                else if (query != 0)
                {
                    Console.Write("Exito");
                    Console.ReadKey();
                }

            }
        }

        
    }
}
